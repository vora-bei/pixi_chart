
import './style.css'
import * as PIXI from 'pixi.js'

const border = 40;

const axisWidth = 2;
const axisColor = 0xffffff;
const backgroundColor = 0x061639;
const lineColor = 0xbbbb00;
const querySelector = '#app';
const multiplicity = 1;
const lineWidth = 2;
const textSetting = {fontFamily : 'Arial', fontSize: 12, fill : 0xff1010};
const graphicPoint = [
    0, -24,
    40, 40,
    -20, 40,
    -60, -20,
    0, 40,
    0, 80,
    -60, 0, 20, 0
];
const canvasGraphicSettings = {
	xShift:        0,
	yShift:        0,
	xStretch:      1,
    yStretch:      1,
    axisNY:       10,
    axisNX:        10,
	paddingTop:    0,
	paddingBottom: 40,
	paddingRight:  0,
	paddingLeft:   40
};
const app = new PIXI.Application({antialias: true});
app.renderer.backgroundColor = backgroundColor;
document.querySelector(querySelector).appendChild(app.view);
const {width, height} = app.view;
function drawAxis(){
    const { axisNX, axisNY, xShift } = canvasGraphicSettings;
    const lines = []
    for(let i=0; i<= axisNX; i++) {
        const line = new PIXI.Graphics();
        line.lineStyle(axisWidth, axisColor, 1);
        line.moveTo(border, 0);
        line.lineTo(app.view.width, 0);
        const y =i * (app.view.height-border) / axisNX;
        const yText = Math.round((height - canvasGraphicSettings.yShift - y)/canvasGraphicSettings.yStretch).toString()
        let text = new PIXI.Text( yText, textSetting);
        line.y = y; 
        text.x = border - 10 - text.width;
        text.y =y - text.height/2;
        lines.push(line);
        lines.push(text);

    }
    for(let i=0; i<= axisNY; i++) {
        const line = new PIXI.Graphics();
        line.lineStyle(axisWidth, axisColor, 1);
        line.moveTo(0, 0);
        line.lineTo(0, height-border);
        line.x = i * (width-border) / axisNY + border; 
        lines.push(line);

    }
    return lines;
}


function extremum (y: number){
    const pow = Math.floor(Math.log10(Math.abs(y)));
    const minus = (y < 0)? -1: 1;
    return minus * Math.ceil(Math.abs(y) / Math.pow(10, pow))*Math.pow(10, pow)
       
}
function setCanvasGraphicSettings(graphicPoint: number[]) {
	var maxY, minY;
		
	if (graphicPoint.length > 0) {
		maxY = graphicPoint[0];
		minY = graphicPoint[0];
		
		for (let i = 0; i < graphicPoint.length; i++) {
			if (maxY < graphicPoint[i]) {
				maxY = graphicPoint[i];
			} else if (minY > graphicPoint[i]) {
				minY = graphicPoint[i];
			}
        }
        maxY = extremum(maxY);
        minY = extremum(minY);
      	canvasGraphicSettings.xStretch = (width - canvasGraphicSettings.paddingRight - canvasGraphicSettings.paddingLeft) / (graphicPoint.length - 1);
		if (maxY != minY) {
			canvasGraphicSettings.yStretch = (height - canvasGraphicSettings.paddingTop - canvasGraphicSettings.paddingBottom) / (maxY - minY);
		} else {
			canvasGraphicSettings.yStretch = 1;
		}
		
		canvasGraphicSettings.xShift = canvasGraphicSettings.paddingLeft;
		canvasGraphicSettings.yShift = canvasGraphicSettings.paddingBottom - minY * canvasGraphicSettings.yStretch;
        canvasGraphicSettings.axisNX = (maxY - minY) / 10 / multiplicity    
    }
}


function getGraphicPointX(pointIndex: number) {
	return canvasGraphicSettings.xShift + canvasGraphicSettings.xStretch * pointIndex;
}

function getGraphicPointY(yPoint) {
	return height - canvasGraphicSettings.yShift - canvasGraphicSettings.yStretch * yPoint;
}
function drawGraphicUsingBezier(graphicPoint: number[]) {
    const line = new PIXI.Graphics();
	var xStretch = canvasGraphicSettings.xStretch;
	var xStretchSqr = xStretch * xStretch;
	let yA, yB, yC, xA, subYaYb, subYaYc, k, s, xLeft, yLeft, xRight, yRight;

    yA = getGraphicPointY(graphicPoint[0]);
    yC = getGraphicPointY(graphicPoint[1]);
    line.lineStyle(lineWidth, lineColor, 1);
    line.moveTo(getGraphicPointX(0), yA);
	for (let i = 1; i < graphicPoint.length; i++) {
		yB = yA;
		yA = yC;
		yC = getGraphicPointY(graphicPoint[i + 1]);
		
		xA = getGraphicPointX(i);
		
		if (i < graphicPoint.length - 1) {
			subYaYb = yA - yB;
			subYaYc = yA - yC;
			
			if (subYaYb != subYaYc) {
				k = (Math.sqrt((xStretchSqr + subYaYb * subYaYb) * (xStretchSqr + subYaYc * subYaYc)) - xStretchSqr - subYaYb * subYaYc) / (xStretch * (yC - yB));
			} else {
				k = 0;
			}
			
			s = xStretch / 2 * Math.sqrt(1 / (1 + k * k));
			
			xLeft = xA - s;
			yLeft = yA - k * s;
		}		
		if (i == 1) {
			line.quadraticCurveTo(xLeft, yLeft, xA, yA);
		} else if (i < graphicPoint.length - 1) {
			line.bezierCurveTo(xRight, yRight, xLeft, yLeft, xA, yA);
		} else {
			line.quadraticCurveTo(xRight, yRight, xA, yA);
		}
		
		if (i < graphicPoint.length - 1) {
			xRight = xA + s;
			yRight = yA + k * s;
        }
    
    }

    return line;
	
}
setCanvasGraphicSettings(graphicPoint);
drawAxis().forEach((line)=>{
    app.stage.addChild(line);
})
app.stage.addChild(drawGraphicUsingBezier(graphicPoint));

